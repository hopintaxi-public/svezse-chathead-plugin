package com.hopinchathead;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import se.svez.app.R;

public class ChatHeadService extends Service {

    private WindowManager mWindowManager;
    public View mChatHeadView;
    public static final String MAIN_APP_STATE_CHANGE_EVENT = "state.event";
    public static boolean SERVICE_STARTED = false;

    public ChatHeadService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        super.onCreate();

        listenBroadcasts();

        //Inflate the chat head layout we created
        mChatHeadView = LayoutInflater.from(this).inflate(R.layout.layout_chat_head, null);
        mChatHeadView.setVisibility(View.INVISIBLE);

        //Add the view to the window.
        int layout_params;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            layout_params = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layout_params = WindowManager.LayoutParams.TYPE_PHONE;
        }

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            layout_params,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT);

        //Specify the chat head position
        params.gravity = Gravity.TOP | Gravity.START; //Initially view will be added to top-left corner
        params.x = 0;
        params.y = 100;

        //Add the view to the window
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mChatHeadView, params);

        //Drag and move chat head using user's touch action.
        final ImageView chatHeadImage = mChatHeadView.findViewById(R.id.chat_head_profile_iv);
        chatHeadImage.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;


            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int chatHeadWidth = mChatHeadView.getWidth();
                int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if ((Math.abs(initialTouchX - event.getRawX())<5) && (Math.abs(initialTouchY - event.getRawY())<5)) {
                            PackageManager pm = getPackageManager();
                            Intent startAppIntent = pm.getLaunchIntentForPackage(getApplicationContext().getPackageName());

                            startActivity(startAppIntent);
                        }

                        // get side of screen to move to
                        int middle = screenWidth / 2 - (chatHeadWidth / 2);
                        float nearestXWall = params.x >= middle ? screenWidth - chatHeadWidth : 0;
                        params.x = (int) nearestXWall;

                        // animate transition to side
                        ValueAnimator va = ValueAnimator.ofFloat(event.getRawX() - (chatHeadWidth / 2), params.x);
                        int mDuration = 400;
                        va.setDuration(mDuration);
                        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                            public void onAnimationUpdate(ValueAnimator animation) {
                                params.x = Math.round((Float) animation.getAnimatedValue());

                                try {
                                    mWindowManager.updateViewLayout(mChatHeadView, params);
                                } catch (Exception e) {
                                    Log.d("HopinChathead", e.toString());
                                }
                            }
                        });
                        va.start();

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        if (params.x > screenWidth - chatHeadWidth) {
                            params.x = screenWidth - chatHeadWidth;
                        }

                        //Update the layout with new X & Y coordinate
                        mWindowManager.updateViewLayout(mChatHeadView, params);
                        return true;
                }
                return false;
            }
        });

        SERVICE_STARTED = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatHeadView != null) mWindowManager.removeView(mChatHeadView);

        SERVICE_STARTED = false;
    }

    private void listenBroadcasts() {
        final BroadcastReceiver stateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isBackground = intent.getBooleanExtra("isBackground", false);
                Handler handler = new Handler(Looper.getMainLooper()) {
                    @Override
                    public void handleMessage(Message msg) {
                        Log.d("HOPINCHATHEAD", "handleMessage" + String.valueOf(isBackground));
                        if (isBackground) {
                            mChatHeadView.setVisibility(View.VISIBLE);
                        } else {
                            mChatHeadView.setVisibility(View.INVISIBLE);
                        }
                    }
                };
                handler.sendEmptyMessage(1);

                Log.d("HOPINCHATHEAD", "isBackground EVENT: " + String.valueOf(isBackground));
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(stateReceiver, new IntentFilter(MAIN_APP_STATE_CHANGE_EVENT));
    }
}
